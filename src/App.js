import React from 'react';
import Header from './components/Header';
import Grid from './components/Grid-teaser';
import './App.scss';

function App() {
    return (
        <div className="App">
            <Header />
            <Grid />
        </div>
    );
}

export default App;
