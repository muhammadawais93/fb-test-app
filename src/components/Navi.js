import React from 'react';
import { Navbar, Nav, NavDropdown } from 'react-bootstrap';
import { ReactComponent as Logo } from '../logo-1.svg';

function Navi() {
    return (
        <>
            <nav className="navbar">
                <ul className="navbar__list">
                    <li className="navbar__item">
                        <a href="#" title="home" rel="noopener noreferrer">Home</a>
                    </li>
                    <li className="navbar__item">
                        <a href="#" title="about" rel="noopener noreferrer">About</a>
                    </li>
                    <li className="navbar__item logo">
                        <a href="#" title="logo" rel="noopener noreferrer"><Logo /></a>
                    </li>
                    <li className="navbar__item">
                        <a href="#" title="service" rel="noopener noreferrer">Service</a>
                    </li>
                    <li className="navbar__item">
                        <a href="#" title="contact" rel="noopener noreferrer">Contact</a>
                    </li>
                </ul>
            </nav>
            <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark" className="mobile-nav">
                <Navbar.Brand href="#"><Logo /></Navbar.Brand>
                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="mr-auto">
                        <Nav.Link href="#features">Home</Nav.Link>
                        <Nav.Link href="#pricing">About</Nav.Link>
                        <Nav.Link href="#pricing">Service</Nav.Link>
                        <Nav.Link href="#pricing">Contact</Nav.Link>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
        </>
    )
}

export default Navi;
