import React from 'react';
import Food from '../images/food.png';
import Pappy from '../images/pappy.png';
import Girl from '../images/girl.png';

function Grid() {
    return (
        <div className="grid-teaser">
            <div className="text-content">
                <h2>I am open sans extra bold 48px</h2>
                <p>Please follow all directions, make fonts the same size, respect margins and spacing.</p>
            </div>
            <div className="image-content">
                <img src={Food} alt="food" />
                <img src={Pappy} alt="dog" />
                <img src={Girl} alt="girl" />
            </div>
        </div>
    )
}

export default Grid;
