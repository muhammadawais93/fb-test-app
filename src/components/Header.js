import React from 'react';
import Navi from './Navi';
import { Parallax, Background } from 'react-parallax';

function Header() {
    return (
        <header>
            <Navi />
            <Parallax
                blur={0}
                bgImage={require('../images/fitness.jpg')}
                bgImageAlt="the cat"
                strength={500}
                className="header-over"
            >
            <div className="header-over">
                <h1>I am Open Sans 120px</h1>
            </div>
            </Parallax>
        </header>
    )
}

export default Header;
